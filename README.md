# Access Now Helpline
# End-User Guides

The end-user guides developed by [Access Now Digital Security Helpline](https://accessnow.org/help) are available at [https://guides.accessnow.org](https://guides.accessnow.org).


### Adding a new link to the sidebar

1. Create your new article using another article as a template, and give it a `tag` that closely matches the title or subject matter. An example of a good tag is "safer_travels" for the "Guide to Safer Travel" article.
2. Go to the `accessnow/pages/tags/` folder in the accessnowguides repository and create a new file. On the Gitlab website, you can create a new file in the folder by clicking the '+' symbol and selecting 'New File' under 'This directory'.
3. Name the new file 'tag_name.md'. 
4. Enter the following template text into the file, then make sure to fill out each field appropriately:
   ```
   ---
   title: "Article Title"
   search: exclude
   tagName: tag_name
   permalink: tag_name.html
   sidebar: mydoc_sidebar
   folder: tags
   ref: name_tag
   lang: en
   langNow: en
   ---



   {% include taglogic.html %}

   {% include links.html %}
   ```
   **NOTE:** The `tagName` field should match the `tag` field in the YAML front matter section of your article.
5. Save the file.
6. Next, go to the `/_data/sidebars` directory and open the `mydoc_sidebar.yml` file for editing.
7. Below line 30, you'll see a section for each tag/link that appears on the guides.accessnow.org sidebar. Add a new section using the following template, and make sure to fill out each field to match the new article:
   ```
       - title: Article Title
         url: /tag_name.html
         output: web, pdf
         lang: en
   ```
8. Save the file.
9. Finally, if you haven't already, make sure to add a link to the front page by editing `index.md`.
10. If you're working from your own fork or branch, add the files to be committed using `git add filename`, then commit the changes with `git commit`, and push the changes to the Gitlab version of your branch or fork using `git push origin branchname`. Then, follow the link in the terminal to open a merge request on Gitlab.
11. Once the merge has gone through, watch the CI process by going to Gitlab, clicking on CI/CD on the sidebar, clicking on the most recent pipeline's number, clicking Jobs, then clicking the Job ID number.
12. If there is an error during the pipeline job, try fixing it by checking the line in the file referred to in the error message, or by searching the internet for the error message. If the job is successful, verify the changes went through properly by checking the guides website.

