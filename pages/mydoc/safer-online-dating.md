---
title: Guide for Safer Online Dating
keywords: dating, online dating
last_updated: March 10, 2021
tags: [dating]
summary: "This guide contains tips and resources for using online platforms more safely in dating, to prevent malicious actors from using information you share for publishing, blackmailing or other forms of harassment."
sidebar: mydoc_sidebar
permalink: safer-online-dating.html
folder: mydoc
conf: Public
ref: safer-dating
lang: en
---

*Please consider the date when this article was last updated by looking at the bottom right corner of the page when evaluating the accuracy and security of the following guide.*


# Access Now Digital Security Helpline
# Safer Online Dating Guide

## The threats of online dating

- **Risks for privacy.** Any kind of social media account carries some privacy risks, but online dating platforms present very specific threats, due to the fact that their users are encouraged to share sensitive personal details publicly with people they don’t know. For example, while a person may not necessarily share their sexual identity publicly on their Facebook page, they may be compelled to do so on an online dating platform so that the platform can connect them with potential matches.

- **Geolocation.** Dating websites and apps often have a geolocation feature, allowing users to see other users near them.  This feature provides an opportunity for people with harmful intentions to identify potential targets nearby.

- **Catfishing.** Catfishing is the act of creating a false identity to pretend to be someone else online. The person who creates the false identity will interact with a target for abusive and deceptive purposes, such as to harass, kidnap, blackmail, arrest, or hurt them in some way. It is a tactic that has been used by criminals, police, and other adversaries to instill fear in and prey on marginalized communities.


## General recommendations

### Online:

- Be careful disclosing your personal information on your online dating profile. Do not share your real name, workplace, social media accounts, or personal and work email addresses.

- Know that you are not obligated to share personal information about your gender, body, or surgical and medical records. Disclosing this information is a personal decision that should be made based on what makes you feel safer and more comfortable.

- Use unique and complex passwords, and store them securely using a [password manager.](https://ssd.eff.org/en/module/creating-strong-passwords#0)

- Never share financial information.

- Verify information about the person using other social media accounts. Which information is shared in their public profile? Is it similar to the information provided in your conversations with them? Are there any real pictures of the person?

- Use video-messaging services. This can be helpful in determining whether they are representing themselves honestly on their profile and in their conversations with you.

- Do not use dating sites or apps on unprotected Wi-Fi networks (public Wi-Fi networks, Wi-Fi networks without a password, etc.).


### Offline:

- Trust your intuition. If it does not seem like someone you are messaging is using their true identity, they probably are not. In this case, do not agree to meet them in person.

- If you feel comfortable with agreeing to an in-person meeting with someone you do not yet know, plan for your first meeting to be in public.

- Tell trusted friends and/or family about your offline meeting plan. Tell them where you're going and when you expect to be home, and come up with a plan of action in case they don't hear from you after a certain time.

- Organize your own means of transport to the meeting, and do not accept a ride from a stranger.

- Don't leave food or beverages unattended.

- Stay sober.


### App-specific:

- Disable showing distance in profile settings (Hornet, Grindr).

- Disable account sharing (Hornet).

- Turn on Stealth Mode (Taimi), or use in-app settings to control who can see your profile (OkCupid).

For more information on app-specific security measures, check our References section at the end of this guide.


## Avoid Catfishing

The number of catfishing cases documented by LGBTQ2IA+ individuals using dating services (such as Grindr, Hornet, etc.) has notably increased in the past few years.

Catfishing involves an adversary creating a fake profile to connect with a target, gain their trust, find out personal details, and then use this information for harmful purposes. They may also be able to install spyware or ransomware on their target's device, for example through a malicious link or attachment.


### Some potential risks for those targeted by catfishing include:

- Harassment, either online or offline

- Blackmailing

- Doxxing (see our [Self-Doxing Guide](https://guides.accessnow.org/self-doxing.html) for more information)

- Financial scams

- Online or offline stalking by a stranger, or a former or current intimate partner

- Physical abuse

- Kidnapping

- Arrest


### The following recommendations can help protect you from catfishing:

- Pay attention to a profile’s creation date and specified details. A new empty profile is probably fake.

- Do not share information about your financial status or workplace.

- Verify their photos. If you are not sure whether someone is who they say they are, you can conduct a [reverse-image search](https://guides.accessnow.org/self-doxing.html#image-searches) based on the pictures they are using. If these are linked to someone else’s profile, the person you are corresponding with may be using a fake profile.

- Meet in a neutral public place. Individuals with harmful intentions usually insist on meeting in spaces they can control.

- If you are already in their space and you feel unsafe, call for help. Consider calling the police if you feel comfortable with it. If the regional context supposes the risk of punishment for being a LGBTQ2IA+ community member, report the incident to any local LGBTQ2IA+ organization providing legal support.

- Try to check the passport of the person you meet to verify their age. Offenders may try to organize a meeting between the target and an underage individual, document the encounter, and use this information to blackmail their target.


## References

**Bumble**
- [Safety Tips](https://bumble.com/the-buzz/safety)

**Butterfly**
- [Safety Tips](https://butterfly.dating/safety/)

**Grindr**
- [Safety Tips](https://help.grindr.com/hc/en-us/articles/217955357-Safety-Tips)

**HER**
- [HER Community Guidelines](https://weareher.com/community-guidelines)

- [How to deal with scammers on HER](https://weareher.com/how-to-deal-with-scammers-on-her)

**Hinge**
- [Safety Tips](https://hingeapp.zendesk.com/hc/en-us/articles/360007194774-Safe-Dating-Advice)

**Hornet**
- [Safety Tips](https://hornet.com/community/knowledge-base/tips-on-how-to-stay-safe/)

- [Here’s How Gay Men Can Avoid Police Entrapment in Egypt in 24 Steps](https://hornet.com/stories/egypt-gay-safety/) – This article focuses on recommendations for LGBTQ2IA+ people in Egypt, and can be helpful also for users in other countries were the police engages in entrapment of individuals through dating apps.

- [Can My Position Be Triangulated By Other Users?](https://hornet.com/community/knowledge-base/can-my-position-be-triangulated-by-other-users-3/)

- [How Do I Go Offline Or Invisible?](https://hornet.com/community/knowledge-base/how-do-i-go-offline-or-invisible-4/)

- [How Can I Disable Others Sharing My Profile?](https://hornet.com/community/knowledge-base/how-can-i-disable-others-sharing-my-profile-2/)

**OkCupid**
- [Safety Tips](https://www.okcupid.com/legal/safety-tips)

- [Safety Tips for OkCupid Users](https://www.lifewire.com/online-dating-safety-tips-for-okcupid-users-2487739) – Andy O'Donnell, Lifewire

**Planet Romeo**
- [Safety Tips](https://www.planetromeo.com/en/care/online-dating/)

**Scruff**
- [Gay Travel Advisories](https://www.scruff.com/en/resources/travel)

**Taimi**
- [Stealth Mode and how to enable it](https://taimi.com/support/answer/9)

- [Use Taimi Protect to enable passcode protection](https://taimi.com/support/answer/54)

**Tinder**
- [Safety Tips](https://policies.tinder.com/safety/intl/en)

**Information on specific contexts**
- [Apps, arrests and abuse in Egypt, Lebanon and Iran](https://www.article19.org/apps-arrests-abuse-egypt-lebanon-iran/) – ARTICLE 19

**More information**
- [Mozilla's 2021 *Privacy Not Included Guide](https://foundation.mozilla.org/en/privacynotincluded/categories/valentines-day/)

- [Trans Lifeline's Resources around online harassment and online dating](https://translifeline.org/resource/online-harassment/)

***If you cannot find a solution in our guide, you can contact the [Access Now Digital Security Helpline](https://www.accessnow.org/help/) team for assistance.***
