---
title: Encrypted Email Set-up for Windows
keywords: email, encrypted email, PGP, GPG, GnuPG, Enigmail, Thunderbird, OpenPGP, Windows, GPG4Win
last_updated: April, 2019
tags: [pgp]
summary: "This guide will help you set up encrypted email on Windows."
sidebar: mydoc_sidebar
permalink: PGP_Encrypted_Email_Windows.html
folder: mydoc
conf: Public
ref: PGP_Encrypted_Email_Windows
lang: en
---

Encrypted Email Set-up for Windows
====================


*Please consider the date when this article was last updated by looking at the bottom right corner of the page when evaluating the accuracy and security of the following guide.*

For encrypting emails on Mac OS X, click [here](https://guides.accessnow.org/pgp/PGP_Encrypted_Email_Mac.html). For encrypting emails on Linux, click [here](https://guides.accessnow.org/pgp/PGP_Encrypted_Email_Linux.html). The [Access Now Helpline](https://www.accessnow.org/help/) is at your service if you have any questions.

## Note on Usage

This guide is recommended for a specific use case, in which the user only uses **Thunderbird as a tool for email encryption**, while reading and sending unencrypted email through their webmail interface in a browser or through a different client.

You can still use Thunderbird as your regular mail client, and we recommend it if you prefer to download your email to a secure device instead of leaving it in the servers of your email provider. For this latter case, we will recommend alternative settings in the relevant sections.

## What You Will Get from This Guide

- The ability to send and receive encrypted emails
- The ability to digitally sign your emails
- The ability to make sure that you are encrypting your email only for the intended recipient/s

## Before Beginning

In order to better protect the data in your computer, we highly recommend enabling device encryption before starting this guide. If your version of Windows supports it, we recommend you enable [BitLocker encryption](http://windows.microsoft.com/en-us/windows-8/bitlocker-drive-encryption). Otherwise, please get in touch with [Access Now Digital Security Helpline](https://www.accessnow.org/help/) to learn how to best secure your device.


Download and Install Gpg4win
-----------------------------

If you want to send and receive encrypted emails, you will first need to download and install Ggp4win, the encryption manager that enables your computer to encrypt and decrypt.

1. Go to [Gpg4win's website](https://www.gpg4win.org/) and click the green **Download Gpg4win** button.

    ![](images/windows/gpg4winWebsite.PNG)

2. Select an option for a free download ("$0") or for a donation, and click "Download".

    ![](images/windows/gpg4winDownload.png)

3. Click "Save File" in the prompt that pops up.

    ![](images/windows/winPGPgpg4winSave.PNG)

4. Verify the file you have downloaded, which will be called `GPG4win-version.exe`, following the below steps:

	- Hover over 'SHA256' in the [download page](https://www.gpg4win.org/download.html) below the download button, to visualize a sequence of numbers and characters.
	
	  ![](images/windows/SHA256Verification.PNG)
	
	- In your machine, open the Command Prompt.
	
	- Enter the command below:

		      certutil -hashfile \path\to\gpg4win-version.exe SHA256

        The output should be the same as the number you visualized when hovering over 'SHA256' in the Gpg4win download page. If the sequence of numbers and characters is not the same, something wrong happened with your download. For example, the file could be damaged, or the download might be incomplete. **Please do not move on to the next steps before having completed the verification correctly.**

5. Once you have correctly verified the file, you can proceed with the installation: click the download arrow in the top right corner of your browser and then click the GPG4win.exe file in your recent downloads.

    ![](images/windows/winPGPDownloadsPGPArrow.PNG) 

6. Click "Yes" when you see the pop-up asking "Do you want to allow this app to make changes to your device?"

7. The Gpg4win setup wizard will appear:

	- Click "Next" in the first page of the wizard.
	
        ![](images/windows/winPGPgpg4winInstall.PNG)
	
	- Pick components you wish to download. The components checked in the screenshot below will allow you to encrypt both emails and files stored on your computer.
   
		- **Uncheck GpgOL**, unless you use Outlook (not recommended), as well as **Browser integration**.

	    - Finally, click "Next".
	
            ![](images/windows/winPGPgpg4winChooseComponents.PNG)
	
	- Select a destination folder - using the default is fine. Click "Install".
	
        ![](images/windows/winPGPgpg4winInstallLocation.PNG)
	
	- In the "Installation Complete" page, click "Next".
	
        ![](images/windows/winPGPgpg4winInstallComplete.PNG)
	
	- Click "Finish" in the final page of the setup wizard.
	
        ![](images/windows/winPGPgpg4winSetupComplete.PNG)


Download and Install Thunderbird
--------------------------------

Thunderbird is an email client that can be used to download and send email, and that you can use together with an add-on called Enigmail to encrypt your email.

1. Visit [Thunderbird's website](https://www.thunderbird.net/) to download Thunderbird.

2. Click the green "Free Download" button.

    ![](images/windows/winPGPThunderbirdDownload.PNG)
   
3. When prompted, select "Save File".

    ![](images/windows/winPGPThunderbirdDownloadSave.PNG)
   
4. Click the download arrow in the top right corner of your browser. Click the "Thunderbird Setup version .exe" file in your recent downloads.

    ![](images/windows/winPGPThunderbirdRecentDownload.PNG)

5. Click "Yes" when you see the pop-up asking "Do you want to allow this app to make changes to your device?"

6. Complete the Thunderbird Installation Wizard:

    * Click "Next" in the Setup Wizard welcome page.

        ![](images/windows/windowsPgpThunderbirdInstallWizard1.png)

    * Choose the "Standard" installation and click "Next".
   
        ![](images/windows/winPGPThunderbirdSetupType.PNG)
      
        *Note: Thunderbird will be set as your default mail application. If you do not wish for this to be the case, **uncheck the "Use Thunderbird as my default mail application"** option at the bottom of this screen.*

    * Click "Install" to allow Thunderbird to be installed in the selected directory.
   
        ![](images/windows/winPGPThunderbirdInstallLocation.PNG)

    * Click "Finish" to complete the installation. Make sure to launch Thunderbird before continuing to the "Download and Install Enigmail" Section. 
   
        ![](images/windows/winPGPThunderbirdCompleteSetup.PNG)
 
*Note: When Thunderbird is launched for the first time, it will ask if you would like to set up an existing email account. We will explain how to set up your email account in Thunderbird in the ["Link Thunderbird to Your Existing Email Account" section below](#setup_email_tb). You can click "Cancel" to continue following this guide, or, if you know what you're doing, set up your existing email account now.*


Download and Install Enigmail
-----------------------------

Enigmail is an add-on for Thunderbird to encrypt, decrypt and sign emails, and to manage PGP keys.

1. Open Thunderbird and **click the three-bar menu button** in the top-right corner.

    ![](images/windows/winPGPThunderbirdMenuBar.PNG)

2. Click "Add-ons" → "Add-ons".

    ![](images/windows/winPGPThunderbirdMenuBarAddOns.PNG)

3. Click "Extensions" in the left-hand menu, type "Enigmail" in the search field in the top-right corner and hit Enter. The first result should read "Enigmail" - click the "Add to Thunderbird" button.

    ![](images/windows/winPGPEnigmailSearch.PNG)
   
4. In the Software Installation prompt, click "Install Now". 

    ![](images/windows/winPGPEnigmailInstallNow.PNG)

5. When the installation of Enigmail is done, a notification will appear in the top left corner. Restart Thunderbird to let the add-on finalize its installation.

    ![](images/windows/winPGPEnigmailSuccessfullyInstalled.PNG)


<a name="setup_email_tb"></a>

Link Thunderbird to Your Existing Email Account
---------------------------------------

*Optional:* Show the Thunderbird Menu Bar. *This step will be useful for the rest of this guide.*

In Thunderbird, click the three-bar menu button, then Options and  check "Menu Bar".

![](images/windows/winPGPCheckMenuBar.PNG)


1. Add your existing email account to Thunderbird by navigating to Thunderbird's menu bar. Click "File", then click "New", then "Existing Mail Account".

    ![](images/windows/winPGPExistingMailAccount.PNG)

2. Fill out your complete name, your email address and your password. Then uncheck "Remember password".

    *It’s important to fill out your first and last name if you want the receiver to recognize you. But this is not mandatory: you can enter anything you would like to be addressed with.*

    Please note that if you have enabled 2-factor authentication on your email account, you might have to enter an app-specific password here. This won't be necessary if you are using a Gmail account.

    ![](images/windows/winPGPExistingMailInput.PNG)

    *Note: if you prefer Thunderbird to store your password, you should make sure that the password database is encrypted, so that nobody who has access to your computer can get to your email password. To do this, you will need to set a strong master password, following [these instructions](https://support.mozilla.org/en-US/kb/protect-your-thunderbird-passwords-master-password).*

3. Click "Continue". Thunderbird will check that your email configuration is correct and will present you with two options for reading your incoming email: IMAP or POP3.

    Leave "IMAP (remote folders)" selected if you want to access the same messages from multiple devices, leaving your email in the servers.

    If you prefer to use Thunderbird for all your email, and to download your email to a trusted secure device rather than keep it in servers you don't control, select the "POP3 (keep mail on your computer)" option. Please note that by choosing this option, the messages you have downloaded will no longer be accessible from other devices. For more details on the IMAP and POP3 options, check out this [Wikipedia page](https://en.wikipedia.org/wiki/Post_Office_Protocol#Comparison_with_IMAP).

    ![](images/windows/winPGPExistingMailConfig.PNG)

    **Important: Make sure that both the Incoming and Outgoing information shown on the screen above display SSL (Secure Sockets Layer) or STARTTLS (Start Transport Layer Security). Either one indicates that your connection to your email provider will have a basic layer of encryption.**

If everything has worked fine, by clicking the "Done" button Thunderbird will connect to your email server and download your mailbox. This operation will happen in the background and might take some time, depending on the size of your inbox folder and speed of your internet connection.

*If you are using a Gmail account, you will be prompted to enter your password and 2-factor authentication code and to accept the connection to an email client.*


Create a Private and Public Key: Your PGP Key Pair
----------------------------------------------

Once your email account is connected to Thunderbird, you can start using PGP by creating a key pair, consisting in a **private key**, which you must **store safely and never share with anyone**, and a **public key**, which you can share publicly or with trusted people.

1. In the Thunderbird menu bar, click "Enigmail", then select "Key Management".

    ![](images/windows/winPGPEnigmailKeyManagement.PNG)

2. Enigmail's key manager window will appear. Select "Generate" from the top menu and click "New Key Pair".

    ![](images/windows/winPGPGenerateNewKeyPair.PNG)

3. Choose a strong and unique passphrase for your key (if you don't know how to generate a strong password, please read [this guide](https://securityinabox.org/en/guide/passwords/)). In a nutshell, your passphrase should be at least 20-characters long and include special characters, capital letters and numbers.

    In the "Key expires in" box, you can leave the default 5 years option, or select a shorter time span. Don't choose a longer time span, and **leave the "Key does not expire" option unchecked**.

    ![](images/windows/winPGPGenerateOpenPGPKeyPage.PNG)

4. Click on the "Advanced" tab next to "Key Expiry", and **make sure the key size is 4096** and the **key type is RSA**.

    ![](images/windows/winPGPConfirmKeySize4096.PNG)

5. Click "Generate Key" and confirm to generate your key pair.

    ![](images/windows/winPGPConfirmGenerateKey.PNG)


Generate a Revocation Certificate
---------------------------------

While Enigmail is generating your key pair, you will also be asked to generate a revocation certificate. Generating a revocation certificate is important and you should click on the "Generate Certificate" button in the prompt, and save the certificate in a secure location (like an encrypted USB stick that you keep for exclusive, personal use).

![](images/windows/winPGPGenerateRevocationCertificateAsk.PNG)

This file is needed only to revoke your key pair in case you lose your private key or there are doubts about it being compromised. The revocation certificate cannot be used to decrypt your PGP-encrypted communications. 

The revocation certificate will allow you to securely discontinue the usage of your key at any time. It is very important to create a revocation certificate for future use and store it in a secure place that only you or trusted people can access and that is separate from the device where you keep your key pair. Revoking your key will prevent people from encrypting to the unused or compromised key, and signals to the keyservers that the key is no longer valid.


Generate a Revocation Certificate for an Existing Key Pair
----------------------------------------------------------

If you did not create a revocation certificate while generating your key pair, you should do so now, following the steps below:

1. Go to the Thunderbird menu bar, click "Enigmail", then choose "Key Management".

    ![](images/windows/winPGPEnigmailKeyManagement.PNG)

2. Find the key that you would like to generate a revocation certificate for, and right-click it. 

3. Select "Generate & Save Revocation Certificate" from the drop-down menu.

    ![](images/windows/winPGPKeyManagementGenerateRevocationCertificate.PNG)

4. Choose a secure location to save your revocation certificate. An encrypted USB stick that you do not lend out and is kept for exclusive, personal use is a good choice. Make sure to keep the storage device where you have saved the certificate in a secure place.

5. Type in the passphrase for your PGP key and click "OK".

    ![](images/windows/winPGPEnterPassphrase.PNG)

6. You will be notified that the revocation certificate has been successfully generated. Click "Close".

    ![](images/windows/winPGPGenerateRevocationSuccessful.PNG)


Back up Your Key Pair Somewhere Safe
------------------------------------

Backing up your key pair in a safe location is important to be sure you will not lose it even if your device is lost or your hard disk damaged, or if you want to encrypt your emails with the same key pair on another computer.

1. Open the Key Management window.

    ![](images/windows/winPGPEnigmailKeyManagement.PNG)
      
2. Right-click your key in the Key Management window and choose "Export Keys to File" in the drop-down menu.
   
    ![](images/windows/winPGPExportKeystoFile.PNG)
   
3. Choose explicitly to "Export Secret Key".

    ![](images/windows/winPGPExportSecretKeys.PNG)
   
4. Choose where you would like the keys to be saved. 

    *Note: The private key in your key pair is the most important component of the encryption system, and its security should be top priority. **Only export your key pair to a secure place**, like an encrypted USB stick that you use exclusively, store in a secure place, and do not lend out.*

    ![](images/windows/winPGPExportKeytoUSB.PNG)

5. Upon clicking "Save", you will be asked to type in the passphrase for your PGP key and click "OK". You should see a message stating that "The keys were successfully saved".

    ![](images/windows/winPGPKeysSuccessfullySaved.PNG)
    

Publish Your Public Key
-----------------------

Your contacts will need to import your public key to encrypt messages to you. To let others find your public key, you may upload your public key to a key server or [attach the key in an email](https://www.enigmail.net/documentation/Key_Management#Share_your_public_key_manually). Public keys uploaded to key servers are searchable by the email address used or PGP Key ID.

**Warning:** If you don't want to disclose publicly that you're using encryption, for example because encryption is illegal in your country, you should avoid publishing your public key on the key servers. In such cases it's a good idea to only send your public key by email or through other means to your trusted contacts you exchange encrypted emails with.

To upload your public key to a keyserver, follow these instructions:

1. Go to Thunderbird's menu bar. Click "Enigmail", then "Key Management".

    ![](images/windows/winPGPEnigmailKeyManagement.PNG)

2. Right-click your key and choose "Upload Public Keys to Keyserver" in the drop-down menu.

    ![](images/windows/winPGPUploadKeystoKeyserver.PNG)

3. Wait for the key upload to finish.

    ![](images/windows/winPGPKeyUploadtoKeyserver.PNG)

<a name="update-thunderbird-settings"></a>

Update Thunderbird Settings
---------------------------

Before you start encrypting your email, it is important to tweak some setting in Thunderbird, by following the steps below.


### OpenPGP Security

1. Navigate to Thunderbird's menu bar, select "Tools", then "Account Settings".

    ![](images/windows/winPGPThunderbirdAccountSettings.PNG)

2. Click "OpenPgp Security" in the left-hand menu. All "Message Composition Default Options" should be checked, to encrypt messages and drafts and sign messages by default. Make sure "Use specific OpenPGP key ID" is selected, with your public key ID below it. Also make sure that "Prefer Enigmail (OpenPGP)" is checked. Click "OK".

    ![](images/windows/winPGPSecuritySettings.PNG)

    *If you are not reading and sending unencrypted email from a webmail interface, and are using Thunderbird for managing all your email, you might want to uncheck the "Encrypt messages by default" option, to make it easier to write unencrypted messages. If you choose to uncheck this box, you should always make sure that the lock icon in your email form is highlighted as in the screenshot below when writing an encrypted email.*

    ![](images/windows/winPGPLockIcon.PNG)


### Autocrypt

Autocrypt is part of the default configuration of Enigmail.

Autocrypt aims at making encryption easier, but if it isn't properly configured, it may disable encryption automatically for some email addresses.

To set up Autocrypt properly, **it is important to change some settings** by following these instructions:

1. Navigate to Thunderbird's menu bar, select "Tools", then "Account Settings".

    ![](images/windows/winPGPThunderbirdAccountSettings.PNG)

2. Click "OpenPgp Security" in the left-hand menu and select the "Autocrypt" tab (to the right of "Message Composition"). Check the following options:

    * Enable Autocrypt
    * Prefer encrypted emails from the people you exchange email with
    
    ![](images/windows/winPGPAutocrypt.PNG)

 
### Enigmail Preferences

1. In Thunderbird's menu bar, go to Enigmail → Preferences, and verify your **passphrase is remembered** for at least **30** and not more than **60** minutes of inactivity.

    ![](images/windows/winPGPEnigmailPreferences1.PNG)
 
2. Click the "Display Expert Settings and Menus" button in the Preferences window, and select the "Sending" tab.

    - in the "General Preferences for Sending" section, check the "Manually configure encryption settings" option.
    - In the "Confirm before sending" section, check "If unencrypted".
    
    ![](images/windows/winPGPEnigmailPreferences2.PNG)

    *This step is optional if you are using Thunderbird to send unencrypted email, but please remember to make sure that the lock icon in your email form is highlighted as in the screenshot below when writing an encrypted email.*

    ![](images/windows/winPGPLockIcon.PNG)

3. Go to the "Key Selection" tab and uncheck the "By Per-Recipient-Rules" option.

    ![](images/windows/winPGPEnigmailPreferences3.PNG)

<a name="subject-encryption"></a>

### Subject Encryption

Enigmail can also encrypt the subject of your message (please note that other metadata will still be visible).

If the subject is encrypted, then the visible subject is replaced with a dummy text like "Encrypted Message". It is a good practice to encrypt your subject by default, but please note that when writing to help@accessnow.org you should disable this feature.

In order to disable subject encryption, go to Enigmail → Preferences → click "Display Expert Settings and Menus", then go to the "Advanced" tab and uncheck the "Encrypt subject by default" option. Please, remember to do this every time you send encrypted emails to help@accessnow.org.
    
   ![](images/windows/winPGPEncryptSubject.PNG)


### Disable HTML and remote content

Email messages can contain content such as scripts in the HTML code, images, or stylesheets that can violate your privacy or be used for malicious intents. To protect your privacy and to mitigate the risk of attacks or vulnerabilities, it is always a good idea to disable HTML composition and rendering, and to block remote content.

#### Disable HTML email composition

1. Navigate to Thunderbird's menu bar, select "Tools", then "Account Settings".

    ![](images/windows/winPGPThunderbirdAccountSettings.PNG)

2. Select the "Composition & Addressing" tab and uncheck the "Compose messages in HTML format" option.

    ![](images/windows/windowsPgpThunderbirdCompositionAddressing.png)

#### Disable HTML email rendering

1. Click the three-bar menu button in the top-right corner of the main Thunderbird window and click View → Message Body As → Plain Text.

    ![](images/windows/windowsPgpTBViewPlainText.png)

#### Disabling remote content in messages

*Remote content should be disabled by default in Thunderbird, but it's a good idea to check.*

1. Navigate to Thunderbird's menu bar, select "Tools", then "Options".

    ![](images/windows/windowsPgpTBOptions.png)

2. Select the "Privacy" tab and make sure that the "Allow remote content in messages" is unchecked.

    ![](images/windows/windowsPgpTBremotecontent.png)
    

Add Your PGP ID to Your Email Signature
---------------------------------------

1. Navigate to Thunderbird's menu bar, select "Tools", then "Account Settings".

    ![](images/windows/winPGPThunderbirdAccountSettings.PNG)

2. Find your PGP key under "OpenPGP Security" and copy it. 

    ![](images/windows/winPGPSecuritySettingsFindPGPKey.PNG)
   
3. In the left-hand menu, click on your email address, then paste your public key into the box for the Signature text.

    ![](images/windows/winPGPSignatureText.PNG)


Send Your First Encrypted Email
------------------------------

### Download Your Receiver’s Public Key

1. Go to Thunderbird's menu bar. Click "Enigmail", then "Key Management".

    ![](images/windows/winPGPEnigmailKeyManagement.PNG)

2. In the menu bar of the Key Management window, select "Keyserver", then "Search for Keys".

    ![](images/windows/winPGPSearchKeyserverKeys.PNG)

3. Search for a key by typing the email address of the receiver or the ID of their PGP key.

    ![](images/windows/winPGPSearchKeyBox.PNG)

4. Select the key you need from the import window and click "OK".

    ![](images/windows/winPGPImportKey.PNG)

5. A message will appear saying that the key has been imported. Click "OK".

    ![](images/windows/winPGPKeyImportedSuccessfully.PNG)

    Once the public key of the receiver is downloaded, you can send them an encrypted email.


### Verify the Fingerprint of the Downloaded Public Key

To be sure that the public key you've just downloaded is the right one, and that nobody has tampered with it, it's a good practice to check its fingerprint.

1. To verify your receiver's PGP key, you need to get its fingerprint directly from its owner. In some cases, you might have your contact's business card with their PGP fingerprint included in it, or your contact may have published their PGP fingerprint in their official website. In other cases you will have to ask your receiver for the fingerprint. This communication should happen over a secure channel alternative to email or in person.

2. Go to Thunderbird's menu bar. Click "Enigmail", then "Key Management".

    ![](images/windows/winPGPEnigmailKeyManagement.PNG)

3. In the "Key Management" window, search for your receiver's address to identify their key.

    ![](images/windows/EnigmailKeyManagementSearch.png)

4. Right-click your receiver's key and select "Key Properties".

    ![](images/windows/EnigmailKeyProperties.png)

5. In the "Key Properties" window for the selected key, you will see the fingerprint of the key. Check that it matches with the key you obtained from your contact. If the fingerprints match, you can now be sure that your receiver will be able to decrypt your email and that nobody has tampered with their PGP key.

    ![](images/windows/EnigmailKeyFingerprint.png)


### Write and Send an Encrypted Email

1. In Thunderbird, click the "Write" button in the top left corner.

    ![](images/windows/winPGPWriteEmail.PNG)

2. Enter the email address of the person you want to write to in the "To:" field and make sure that both the **lock and pencil icons are highlighted**. This shows the email is going to be encrypted and signed. If the icons are not highlighted by default, make sure that you have imported your contact's public PGP key and [review Thunderbird's security settings](#update-thunderbird-settings) before continuing.

    ![](images/windows/winPGPSendEmailSecurityIcons.PNG)

3. Write the "Subject" and your email message. Click "Send" when complete.

    If subject line encryption is enabled in your [Enigmail Settings](#subject-encryption), your subject line will be encrypted by default. If you have left that option unchecked, or have unchecked it to write to help@accessnow.org, **you should be very careful not to write private information in the subject line under any circumstances**.
    
    ![](images/windows/winPGPEncryptedEmail.PNG)

4. After clicking Send, you must fill out your PGP passphrase for signing the message and click "OK".

    ![](images/windows/winPGPEnterPassphrase.PNG)


Extend Your Expired Key
-----------------------

If your key reaches its expiration date, you can always extend it as long as you still have access to the private key. To do so, you can follow these instructions:

1. Go to Thunderbird's menu bar. Click "Enigmail", then "Key Management".

    ![](images/windows/winPGPEnigmailKeyManagement.PNG)

2. In the Key Management window, **right-click** the expired key and select "Change Expiration Date" in the drop-down menu.

    ![](images/windows/winPGPExpiredKey.PNG)
   
    *Note: An expired key can be easily identified in Enigmail, as its font format changes to italics.*
   
3. Make sure that the primary and subkey are checked. Set the expiration date to **5 years at most** and click "OK".

    ![](images/windows/winPGPExpiredKeyRenew.PNG) 
   
4. Type in the passphrase for your PGP key and click "OK".

    ![](images/windows/winPGPExpiredKeyPassphraseEnter.PNG)
   
5. **Right-click** the key you have just extended, and select "Upload Public Keys to Keyserver".

    ![](images/windows/winPGPKeyUpload.PNG)
   
    **Warning:** This step is necessary to keep the public keyring details up-to-date in the publicly accessible keyservers. If you have chosen not to publish your PGP key, you should send the extended key to your contacts instead of spreading the updated key to the servers.
   
6. Wait for the key upload to finish.

    ![](images/windows/winPGPKeyUploadtoKeyserver.PNG)


* * *

This guide was brought to you by [Access Now](https://www.accessnow.org/). If you have any questions or problems, please contact the [Access Now Helpline](https://www.accessnow.org/help/). Our Tech team will be happy to help. 

![](images/mac/ccLicense.png)

**Share** -- Copy and redistribute the material in any medium or format.

**Adapt** -- Remix, transform, and build upon the material.

**Noncommercial** -- You may not use the material for commercial purposes.

