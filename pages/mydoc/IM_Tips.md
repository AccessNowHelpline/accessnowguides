---
title: Tips – Secure IM Apps
keywords: IM, instant messaging, IM apps, WhatsApp, Facebook Messenger, Telegram, Signal, Wire
last_updated: October 2019
tags: [im_tips]
summary: "This is a series of tips to use your IM apps as securely as possible."
sidebar: mydoc_sidebar
permalink: IM_Tips.html
folder: mydoc
conf: Public
ref: IM_Tips
lang: en
---

# Tips – Secure IM Apps

*Please consider the date when this article was last updated by looking at the bottom right corner of the page when evaluating the accuracy and security of the following guide.*

## WhatsApp

These settings are a reasonable middle ground for reducing the amount of data that the app creates locally, and minimizing what is exposed as clear text on iCloud/Google Drive.

1. Go to: Settings -> Account -> Privacy

    Tweak the following settings:

    - Last Seen: My Contacts

    - Profile Photo: My Contacts

    - Status: My Contacts

    - Read Receipts: OFF

2. Go to: Settings -> Account -> Security

    Tweak the following settings:

    - Show Security Notifications: ON

3. Go to: Settings -> Chats

    Tweak the following settings:

    - Save Incoming Media: OFF

    - Chat Backup -> Auto Backup: OFF

4. Go to: Settings -> Notifications

    Tweak the following settings:

    - Show Preview: OFF (unfortunately, this still displays the sender’s name)
    
5. Disappearing messages (Beta version - it might not work on some devices yet and is only active on group chats):

    - Go to the group in WhatsApp where you want to set disappearing messages, then tap the subject of the group. 
    
        Alternatively, tap and hold the group in the CHATS tab. Then tap Menu -> Group info.

    - Tap Group settings -> Disappearing Messages

    - Select one of the available time choices. At the moment the choices are limited: 5 seconds or one hour.


## Telegram

1. Enable 2-factor authentication: 

    - Instructions: [https://telegram.org/faq#q-how-does-2-step-verification-work](https://telegram.org/faq#q-how-does-2-step-verification-work)
    - Enable: Settings –> Privacy and Security –> 2-Step Verification

2. Use username and share it instead of the phone number.

    - Username can be changed. Phone numbers can’t. 
    - However any usernames are searchable and anyone can send you a messages via t.me/username. 

3. Telegram provides groups and user-to-user chat. Both types can be secured.

    - User-to-user chat is not end-to-end encrypted by default. To encrypt it, you need to enable a feature called Secret Chats:

        - More information: [https://telegram.org/faq#secret-chats](https://telegram.org/faq#secret-chats)
        - The Secret Chats feature encrypts communication between two users.
        - The admins of Telegram can’t read it. 
        - If you delete a message, the copy on the other side will be deleted too. 
        - You can set a timer after which a message will be deleted. 
        - These two features are useful in case the device is seized or stolen.
        - You can enable notifications to learn if the other side takes screenshots. But this is not available on all platforms (especially Android).
        - Secret Chat can be started as follows:
            - iOS: Start a new message (tap the icon in the top-right corner in Messages). Then tap "New secret chat".
            - Android: Swipe right to open the menu, then ‘New secret chat’.

4. If you are managing a Group on Telegram:

    - Groups are private by default. They can be turned to public.
    - Group Permissions: It is useful to restrict all members from posting specific kinds of content if inappropriate.
    - The administrator can delete inappropriate content.


## Facebook Messenger

Facebook Messenger can also be used for end-to-end encrypted communications, but this feature is not the default. Secret messages can be used for sending messages, pictures, stickers, videos, and voice messages. This feature cannot be used for group messages and voice or video calls.

To start a so called Secret Conversation:

1. Tap the button to write a message in the top right of the app.
2. Tap “Secret” in the top right.
3. Select who you want to message.
4. If you want, tap the timer icon in the text box and set a timer to make the messages disappear.

Secret conversations are currently only available in the Messenger app on iOS and Android, so they won't appear on Facebook chat or messenger.com.

More information on secret conversations: [https://www.facebook.com/help/messenger-app/1084673321594605/](https://www.facebook.com/help/messenger-app/1084673321594605/)


## Signal

- You can find a good introduction to Signal in this article by Martin Shelton: [https://freedom.press/news/signal-beginners/](https://freedom.press/news/signal-beginners/).
- Secure Signal following these instructions, also by Martin Shelton: [https://medium.com/@mshelton/locking-down-signal-d71678f653d3](https://medium.com/@mshelton/locking-down-signal-d71678f653d3)
- Signal can only be registered through an existing phone number, which becomes the user's identifier. If you prefer not to use your personal phone number or to share it with your contacts, you can register Signal with a burner SIM card (if available in your country), or with an online phone service. You can find instructions on how to do this in this tutorial: [https://theintercept.com/2017/09/28/signal-tutorial-second-phone-number/](https://theintercept.com/2017/09/28/signal-tutorial-second-phone-number/).


## Wire

Wire can be registered with a phone number or an email address. Instructions for registering a Wire account with an email address can be found here: [https://support.wire.com/hc/en-us/articles/360000165369-How-can-I-register-with-an-email-](https://support.wire.com/hc/en-us/articles/360000165369-How-can-I-register-with-an-email-)

Features:

-  1:1 IM, group chat, voice messages, file sharing, voice calls, and 1:1 video calls are all end-to-end encrypted.
-  You can verify the identity of other users' devices
-  You can set a timer for disappearing messages, or delete messages in your recipient's devices
