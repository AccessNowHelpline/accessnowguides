---
title: Guide to Safer Travel
keywords: safer travel, high-risk countries, crossing borders
last_updated: December, 2018
tags: [safer_travels]
summary: "This guide will help you prepare for travels to high-risk countries."
sidebar: mydoc_sidebar
permalink: safer_travel_guide.html
folder: mydoc
conf: Public
ref: safer_travel_guide
lang: en
---

*Please consider the date when this article was last updated by looking at the bottom right corner of the page when evaluating the accuracy and security of the following guide.*

# Access Now Digital Security Helpline
# Guide to Safer Travel

## The threat

Traveling exposes you, your devices, and your data to many risks. Laptops and
other devices can fail or get stolen, lost, damaged, or impounded. Data in
transmission from public access points such as Internet cafes and hotel wifi can
be intercepted. Using untrusted systems may expose you to keyloggers and other
attacks designed to capture information that should be kept secure.

In addition, when you are crossing any national border you have few if any
rights to protest or refuse when a border control or customs officer wants to
inspect any part of your luggage, including the data stored on your laptop,
digital camera, recording device, or cellphone. Even in the US, despite the
Fourth Amendment, a customs officer needs no "probable cause" or warrant to
search your machine or impound it for further inspection.

## The risks

Will you be able to work if your machine and all its data are inaccessible?
Will your co-workers, supporters, aides, allies, or donors be endangered if the
data you're carrying is exposed to outside scrutiny? What will you do if someone
who has intercepted or accessed your data uses what they've learned to
impersonate you, potentially damaging your personal reputation and that of your
project or organization and endangering those who innocently respond?

A small amount of attention to security and safety can avoid all these
consequences.

## The tools

Many tools and practices are available to help you keep your data safe and
secure while traveling at little or no cost.

These include:

- Basic security practices – some measures that are useful to protect your
  digital assets and communications are even more important when travelling.
- Data minimization – travel with as little data as possible.
- Encryption – use (where legally permitted) on desktops, laptops,
  and smartphones to encrypt email, files, and file systems.
- Live USB sticks – run on public-access computers instead of the supplied
  operating system to ensure no data is kept and you are not tracked.
- Circumvention tools - In case of Internet blockages, tools like VPNs and Tor can be used to encrypt your connections and circumvent censorship.
- Temporary email addresses – use to ensure that your main email accounts are
  not compromised.
- Cloud backup systems – use to store your data so you can travel with a minimum
  of data and/or restore a machine that is physically inaccessible during travels.
- Encrypted USB sticks and other flash memory – use to keep important
  (encrypted!) data backed up in situations where online access is uncertain.

### Basic security practices

Some of the following recommendations are general security practices, but they
should be implemented with special care when planning a journey.

-   Whatever measures you can take among the following steps, if your system and
    your software are not up-to-date, they are more vulnerable to attacks.
    Update regularly your operating system and programs.
-   Install an antivirus or update it if you already have one. To learn more,
    you can read [this guide](https://securityinabox.org/en/guide/malware/) on how to protect your devices against malicious software.
-   Avoid opening non requested attachments or suspicious emails to prevent
    phishing attacks and malware infections.
-   Set up multi-factor authentication whenever possible, to minimize the risk
    of someone accessing your email and social media accounts. [This infographic](https://www.accessnow.org/cms/assets/uploads/2017/09/Choose-the-Best-MFA-for-you.png)
    will help you choose the type of multi-factor authentication that is best
    for you. [This guide by the Electronic Frontier Foundation](https://www.eff.org/deeplinks/2016/12/12-days-2fa-how-enable-two-factor-authentication-your-online-accounts) can help you set up multi-factor authentication.
-   Investigate the situation of the country you will be visiting. In
    particular, check if encryption is banned, if the internet is accessible,
    and if there is any law that forces to hand over your encryption keys, your
    email password and other credentials to the border police when entering the
    country.

<a name="data_minimization"></a>

### Data minimization

Data you are not carrying cannot be lost or stolen. Begin by considering what
data you actually need while travelling and what can be left behind. Copy
everything you do not need onto another computer or hard drive that will remain
safe at your home or office. While you are doing this, also back up your
sensitive data onto media such as external hard drives and USB sticks, and store
them in a secure location separate from your main computer such as a small
combination safe in an employee’s residence.

Especially ensure that you are not carrying any data that could be illegal in
any of the countries you are traveling to. Potentially illegal data might
include authorized copies of copyrighted material (such as music and video),
pornography, and even innocent photographs of unclothed minors. If such material
is found in a border search, your laptop could be confiscated.

One option used by some frequent travelers is to store either just their data
(encrypted) or an entire image of their system complete with software with a
cloud service provider and travel with a bare-bones laptop. Once arrived at
their destination, they then download the data and/or software, re-uploading it
before departure. This set-up provides both data minimization and an accessible
backup, although it limits what work you can do in transit.


### Encryption

While it is always advisable to encrypt the data stored on your computer and
communications devices, the import, export, and/or use of cryptography is
illegal in some countries. Check the rules in the specific countries you will
visit before traveling. In some cases you may need to apply for an import or
export permit; in a few cases personal use may be banned entirely. You should
also check on the legal situation with respect to handing over your private keys
to law enforcement: in some countries you may go to jail if you refuse to cooperate.

- [World map of encryption laws and policies](https://www.gp-digital.org/national-encryption-laws-and-policies/)
- [World map showing countries with import, use, or export controls](http://cryptolaw.org/cls-sum.htm)
- [Detailed crypto import/use/export information by country](http://cryptolaw.org/)
- [Some basic information on travel and encryption](https://informationsecurity.princeton.edu/encryption/encryption-and-internatio)

#### Hard disk encryption

Encryption is an important technique for ensuring that if someone gets hold of
your laptop or other device and tries to access your data without your
passphrase all they will see is a random pattern of 1s and 0s. Be aware,
however, that using encryption makes recovery of your data harder if your hard
drive suffers a drop, violent knock, or other form of accident. The costs of
this type of advanced data recovery are out of scope for most civil society
organizations. Keeping good backups is essential.

See the section [Hard drive encryption by platform](#FDE) below to learn how to
enable full-disk encryption in your system.


#### Protecting cryptographic keys

If you encrypt your email, probably the most valuable data on any device you
carry with you will be your PGP/GPG private encryption keys. You should pay
particular attention to protecting them and the passphrase that unlocks them.
This means not allowing such devices to be in the hands of third parties without
supervision and not leaving the devices unattended when others have access to
them. You may prefer not to keep your private keys on your devices at all.
Instead you could keep them on an encrypted USB thumb drive that hangs around
your neck and that you keep under your control at all times.

### Public access

You will frequently have occasion to use public systems when traveling, whether
these are computers provided in Internet cafes or other public areas, or the
wired or wireless networks in hotels, conference centers, and elsewhere. Each of
these situations has its specific risks.

#### Shared computers

##### Trusted keyboard

The most likely way your security will be thoroughly compromised is through
logging into your accounts on a machine or keyboard that is not trusted. If you
are not using your own device, then the keyboard is untrusted.

The particular danger is keyloggers, which capture everything you type into the
computer for later inspection by attackers. These come in two types: hardware
and software.

Hardware keyloggers, which are available for both PS/2 and USB form factors, are
dongles inserted into the cable running between the keyboard and the computer
You should check for these before using any keyboard that is not your own.

Another option is to travel with your own small USB keyboard that you can plug
into untrusted computers without having to reboot.

##### Untrusted systems

Software keyloggers, which may operate at the kernel level, are a much more
dangerous threat because it is so difficult to tell if they are present. For
that reason, gaining any level of confidence in an untrusted computer is a
daunting prospect.

The best solutions to this is a custom live USB that you have prepared with your
choice of operating system and the basic software you need.  This lets you boot
your own trusted operating system from the USB and bypass the untrusted
computer's standard set-up entirely.

#### Untrusted network access

The need to use untrusted connections to the Internet is a constant when
traveling. These include hotel wired and wireless connections, Internet cafes,
municipal and commercial hotspots, and so on. Rogue nodes in such networks may
be able to perpetrate what are known as man-in-the-middle (MITM) attacks. These
can capture the addresses of sites you visit and the addresses and contents of
email communications. A rogue node can also divert your traffic from the genuine site you're
trying to access to a dangerous fake.

Encryption – again – is the most important technology to ensure that data cannot
be read by anyone who intercepts it in transit. A common solution is to use a
virtual private network (VPN) connection to your home network, which sends all
your Internet traffic through an encrypted tunnel to (and then possibly back out
again from) your trusted system.  You can also connect to the internet through a
trusted VPN (see the [section on VPNs](#VPN) below).

HTTPS, which secures connections to specific websites, must be set up as an
available protocol by the particular website you’re using. Most websites enable
HTTPS, and you can install a browser addon like [HTTPS
Everywhere](https://www.eff.org/de/https-everywhere) to make sure that you will
open the HTTPS version of the websites you want to visit whenever it is
available.

Regardless of which encrypted protocol you are using, pay attention if warnings
pop up when you initiate a connection, as such errors may well indicate a MITM
attack. But their absence does not necessarily mean you're safe: some MITM
attacks hijack sessions without triggering an error message. However, successful
session hijacks will generally give attackers only a limited time in which to
perpetrate damage, as they typically grant the attacker only the key for the
current session.

### Internet censorship and blockages

In some countries, the access to the web may be limited due to censorship of
certain websites or to country-wide blockages.

<a name="VPN"></a>

#### VPNs

A VPN encrypts all the data you request from the web and forwards them to
another computer that sends out the requests for you. If the other computer is
located outside of the country that implements the censorship, the VPN can be
used to access censored websites and services while protecting your traffic from
being intercepted locally. But the other computer can keep logs of all your
traffic, which could be traced back to you, and if your adversary is powerful
enough, they could pressure VPN providers to disclose this information.
Therefore it is very important to trust the VPN provider you use. You can read
[this guide](https://ssd.eff.org/en/module/choosing-vpn-thats-right-you) on how
to choose a VPN, or, if travelling to countries at risk, contact [Access Now
Digital Security Helpline](https://www.accessnow.org/help) for specific
recommendations.

#### Tor

Tor stands for "The Onion Router", and it is specifically designed to bypass
censorship and provide anonymity for web browsing and using other Internet
resources such as instant messaging and remote logins in potentially hostile
situations. It is therefore wise to use it when operating abroad, especially if
you're inside a country where the incumbent regime is opposed to the actions of
your organisation.

To browse the web with Tor, you should use the [Tor
Browser](https://www.torproject.org/download/download-easy.html.en). Since Tor
can slow down your connection, it can be a good idea to use it for accessing
sensitive information, while you use your usual browser for general web surfing
to non-sensitive information. However, be careful what you class as sensitive.
Even apparently innocuous websites may actually give away a lot more about you
and your intentions than you may at first realize. Seemingly mundane information
such as your travel plans or making arrangements with people that tip off an
eavesdropper that you will be out of your hotel room for a period of time can
give away information that could be used to your disadvantage. If in doubt, use
the Tor Browser.

Please note that Tor may be blocked in some countries, for example in China and Kazakhstan. To
use Tor in these countries, you can use [Tor
bridges](https://bridges.torproject.org/).

Read [this article](https://blog.torproject.org/blog/breaking-through-censorship-barriers-even-when-tor-blocked) on what to do when Tor is blocked in a country.

#### Alternative email address

Because you will have to use untrusted connections and systems, it's a good idea
to limit the consequences of an attack. One important tactic is to create a new,
web-based email address just for the trip that you will 'throw away' afterwards.
Use this address for all non-sensitive communications while traveling so that if
the account is compromised it will give the attacker no value beyond the end of
your trip. Do not log into your regular email account until or unless you can be
confident that the connection and computer are safe.


### Mobile phone security

#### General recommendations

Mobile phones should generally be considered as insecure. When travelling, it’s
best to leave your phone at home, and to travel with a different mobile device
that does not contain all your data. If you have to take your work phone with
you, back up all your data in a different device and remove them from the phone
and its memory card. To limit the risks your smartphone is exposed to, you can
read [this guide](https://securityinabox.org/en/guide/smartphones/).

##### Censorship circumvention

Many of the tools recommended in this document, like VPNs or Tor, can be used
also through apps that can be installed on Android or iOS devices.

On Android, you need to install
[Orbot](https://guardianproject.info/apps/orbot/) to run Tor and then a browser
called [Orfox](https://guardianproject.info/apps/orfox/), which connects to the
web through Orbot. On iOS, the app of choice for surfing the web over Tor is the
[Onion Browser](https://mike.tig.as/onionbrowser/).

##### Secure communications

While you’re travelling, some of the fastest ways to secure your communications could be offered by smartphones apps. If the people you need to communicate with use encrypted messaging apps like Signal or Wire, you can consider using these pretty secure communication tools to send encrypted messages and files, as well as for encrypted voice calls. Tools like WhatsApp or Facebook Messenger are less recommended but can still be good solutions if encryption is turned on and you aren't using them to discuss very sensitive details.


### Further advice

#### Watch out for shoulder surfers

Always be aware of who is in your immediate vicinity when you type in passwords,
passphrases, and the PINs that protect credit cards and debit cards. Protecting
against "shoulder surfers" who pick up your user IDs and passwords by watching
from behind you or remotely (look for cameras) is the reason why today's
operating systems replace the passwords you type in with a series of dots. But
the same information can be picked up by watching your fingers on the keyboard,
so be careful.

#### Set your device to lock automatically when unattended

Most operating systems provide a way of automatically locking the keyboard and
display after the user has been inactive for a period of time. This is a good
practice even though of course you intend never to leave your device
unattended. Similarly, you should set your device to require a password at
logon and whenever you resume from standby.

#### Creating and managing good passwords/passphrases

If you haven’t defined a general password policy, it is important that you do so
when preparing for travels.

The passwords you use should be unique, meaning that you only use each one for
one purpose (accessing a web account, decrypting a storage device, etc.), and
strong, i.e. at least 12-characters long, and composed by lower- and upper-case
letters, symbols and numbers. This kind of password is very difficult to
remember and to type, and to make sure you don’t lose them, the best solution is
to use a password manager like [KeePassXC](https://keepassxc.org/). This tool
will not only store all your passphrases in an encrypted database, but will also
allow you to copy-paste your passphrases without having to type them, which will
also prevent shoulder-surfing.

But there are some passwords which you cannot keep in your password manager, for
example the passphrase for decrypting your hard disk when starting your computer
and the passphrase to open your encrypted password database. These passphrases
should be easy to type and to remember, and hard to guess. A suitable strategy
is to pick three or more unrelated words and concatenate them with one or more
symbols or numbers. For your encryption key passphrase, create a long and
memorable sentence (at least 6 words); this will naturally have capitals,
spaces, and punctuation and be very hard to guess.

Read [this guide by the Electronic Frontier Foundation](https://www.eff.org/de/dice) on how to create strong and memorable passphrases.

In general, when creating passwords do not use your name, your organization’s
name, any portion of your or your organization’s street address, or the names of
your pets, spouse, or children. Also do not use single words you can find in a
dictionary of any language; these are easily cracked. If you already use strong
and unique passwords, but you have used them in insecure situations, you should
change them afterwards when you’re in a secure situation again.

<a name="FDE"></a>

#### Hard drive encryption by platform

Free, open source software to encrypt your entire hard disk/s is available for
all of the common operating systems, both for computers and mobile devices.

Encrypting your hard drive will protect the information you have stored in it in case your device is lost or stolen.

Please note that **full-disk encryption won't help if someone can force you to hand over the decryption passphrase**, as can happen at certain borders. In these situations, if you want to avoid sensitive information from falling in the wrong hands, you should [not carry that information with you](#data_minimization) or store it in a [hidden encrypted folder created with VeraCrypt](https://www.veracrypt.fr/en/Hidden%20Volume.html).

You can learn how to create a hidden volume with VeraCrypt in these guides for [Windows](https://securityinabox.org/en/guide/veracrypt/windows/#creating-a-hidden-volume), [Mac](https://securityinabox.org/en/guide/veracrypt/mac/#creating-a-hidden-volume), and [Linux](https://securityinabox.org/en/guide/veracrypt/linux/#creating-a-hidden-volume).


##### Mac OS X

Full disk encryption is easiest on Mac OS X devices because it's built into the
operating system and is even the default setting for new devices. In fact, you
may have disk encryption activated without knowing it; when you log onto your
Mac, your account password is the 'secret' that unlocks the private keys that 
are used to decrypt your hard drive.

To check if disk encryption is on or to turn it on for Mac OS X laptops, you can
follow [these instructions](https://support.apple.com/en-us/HT204837).


##### Microsoft Windows

BitLocker is a full disk encryption feature built in in all Windows Pro versions
starting from the Ultimate and Enterprise editions of Vista and Windows 7.

- [A step-by-step guide for Windows
  7](http://technet.microsoft.com/en-us/library/dd835565)
- [A step-by-step guide for Windows
  10](https://blogs.msdn.microsoft.com/mvpawardprogram/2016/01/12/securing-windows-10-with-bitlocker-drive-encryption/)

For versions of Microsoft Windows that do not ship with BitLocker, such as
the home editions of Vista and 7, there are many options including several
free and open source full-disk encryption software applications. We recommend
[VeraCrypt](https://www.veracrypt.fr).

VeraCrypt offers many options for encrypting all or part of your hard drive, including hiding an encrypted portion, but it can be hard to configure it for full-disk encryption. You can find a guide on how to do it [here](https://medium.com/@securitystreak/veracrypt-full-disk-drive-encryption-fde-157eacbf0b61).

In alternative, if you are a member of civil society, you can consider applying to [TechSoup](https://www.techsoup.org/) for getting a discounted version of Windows Pro that will include Bitlocker.

##### Linux

Full-disk encryption is usually offered when you first install Linux in your machine. If it was not set up during the installation, you might need to reinstall your system. Please get in touch with [Access Now Digital Security Helpline](https://www.accessnow.org/help/) if you need help for enabling full-disk encryption in your Linux computer.


##### Mobile devices

On iOS and Android Marshmallow and later versions, full-disk encryption is activated by default. In older versions of Android you can activate full-disk encryption in the Security settings.


## References

- The [Umbrella app](https://secfirst.org/index.html) offers important tips on
  safer travels for people at risk
- [Establish a technical and legal shield against the latest laptop ban
  concerning certain
  airlines](https://www.eff.org/wp/digital-privacy-us-border-2017)
- [Recommendations by the Electronic Frontier Foundation on crossing a
  border](https://ssd.eff.org/en/module/things-consider-when-crossing-us-border)
- [Princeton University International Travel guide is one of the most complete
  sources available](https://informationsecurity.princeton.edu/intltravel)
- [General guidelines about digital security while
  traveling](https://www.accessnow.org/blog/2015/03/17/best-practices-for-digital-security-while-traveling-to-rightscon)


