---
title: "Access Now Helpline Guides RU"
keywords: home
tags: [home]
layout: home
sidebar: mydoc_sidebar
permalink: ru_index.html
ref: home
lang: ru
---


In this website, you can find the end-user guides specifically developed by [Access Now Digital Security Helpline](https://www.accessnow.org/help/) for our beneficiaries' needs.

This website is deployed from a [public repository on Gitlab.com](https://gitlab.com/AccessNowHelpline/guides). We are very thankful for any feedback. If you would like to suggest an edit, improvement, or update for these guides, you can create [an issue](https://gitlab.com/AccessNowHelpline/guides/issues) or do a pull request, or you can just write an email to docs @ accessnow . org.

* * *


- [**Руководство по безопасности поездок**](safer_travel_guide_ru.html)

- [**Как собрать информацию о себе**](self-doxing_ru.html)


{% include links.html %}


