---
title: "Access Now Helpline Guides"
keywords: home
tags: [home]
layout: home
sidebar: mydoc_sidebar
permalink: index.html
ref: home
lang: en
---


In this website, you can find the end-user guides specifically developed by [Access Now Digital Security Helpline](https://www.accessnow.org/help/) for our beneficiaries' needs.

This website is deployed from a [public repository on Gitlab.com](https://gitlab.com/AccessNowHelpline/accessnowguides). We are very thankful for any feedback. If you would like to suggest an edit, improvement, or update for these guides, you can create [an issue](https://gitlab.com/AccessNowHelpline/accessnowguides/issues) or do a pull request, or you can just write an email to docs @ accessnow . org.

* * *

## English

- **PGP: Setting up Thunderbird to encrypt emails**
    - [Windows](PGP_Encrypted_Email_Windows.html)
    - [macOS](PGP_Encrypted_Email_Mac.html)
    - [Linux](PGP_Encrypted_Email_Linux.html)

- [**Guide to Safer Travel**](safer_travel_guide.html)

- [**Guide for Safer Online Dating**](safer-online-dating.html)

- [**Self-Doxing Guide**](self-doxing.html) - how to map public information on oneself to prevent doxing

- [**Tips on how to secure the most common IM apps**](IM_Tips.html)


## Russian

- [**Руководство по безопасности поездок**](safer_travel_guide_ru.html)

- [**Как собрать информацию о себе**](self-doxing_ru.html)

{% include links.html %}


